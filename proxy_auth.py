from selenium import webdriver
import time

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("./proxy.zip")
chrome_options.add_argument("--headless")

driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
driver.get("https://vnexpress.net/")



time.sleep(2)

S = lambda X: driver.execute_script('return document.body.parentNode.scroll' + X)
driver.set_window_size(S('Width'), S('Height'))
driver.find_element_by_tag_name('body').screenshot('vnexpress2.png')
driver.quit()

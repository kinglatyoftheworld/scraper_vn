from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
import time


PATH = './chromedriver'
driver = webdriver.Chrome(PATH)
driver.get('https://vnexpress.net')

time.sleep(3)
link = driver.find_element(By.XPATH, '//h3[@class="title-news"]/a')

source = link.click()

time.sleep(2)
html = driver.page_source

driver.quit()

soup = BeautifulSoup(html, 'html.parser')

title = soup.find('h1', {'class': "title-detail"}).getText()
date = soup.find('span', {'class': 'date'}).getText()

article_url = soup.find('meta', {'property': 'og:url'})['content']
paragraph_list = soup.find('article', {'class': 'fck_detail'}).find_all('p')

try:
    author = soup.find('p', {'class': 'author_mail'}).find('strong').getText()
except:
    author = paragraph_list[-1].getText()

print("Title: ",  title)
print("Date: ", date)
print("Author: ", author)
print("Link: ", article_url)
print("Article: ")
for i in range(len(paragraph_list) - 1):
    paragraph = paragraph_list[i]
    print(paragraph.getText())

# driver.quit()
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

PROXY = 'http://zproxy.lum-superproxy.io:22225'

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--proxy-server=%s' % PROXY)

chrome = webdriver.Chrome( './chromedriver',chrome_options=chrome_options)
chrome.get('https://vnexpress.net')